#include <fmt/format.h>
#include <cufft.h>

class Grid {

  public:
    cufftComplex* c;
    cufftReal* r;
    int N_logical[3];
    int N_real[3];
    int N_complex[3];
    int Ntot_logical;
    int Ntot_real;
    int Ntot_complex;

    Grid(int _N_logical[3]) {
      c = NULL;
      r = NULL;

      memcpy(N_logical, _N_logical, sizeof(int)*3);
      Ntot_logical = N_logical[0] * N_logical[1] * N_logical[2];

      N_complex[0] = N_logical[0];
      N_complex[1] = N_logical[1];
      N_complex[2] = N_logical[2]/2 + 1;
      Ntot_complex = N_complex[0] * N_complex[1] * N_complex[2];

      N_real[0] = N_logical[0];
      N_real[1] = N_logical[1];
      N_real[2] = 2*N_complex[2];
      Ntot_real = N_real[0] * N_real[1] * N_real[2];

    }

    cufftReal& rat(int i, int j, int k) {
      assert(i < N_logical[0]);
      assert(j < N_logical[1]);
      assert(k < N_logical[2]);
      return r[k + N_real[2] * (j + N_real[1] * i)];
    }

    cufftComplex& cat(int i, int j, int k) {
      assert(i < N_complex[0]);
      assert(j < N_complex[1]);
      assert(k < N_complex[2]);
      return c[k + N_complex[2] * (j + N_complex[1] * i)];
    }

};

void print_card_info() {
  int nDevices;
  cudaGetDeviceCount(&nDevices);
  for (int i = 0; i < nDevices; i++) {
    cudaDeviceProp prop;
    cudaGetDeviceProperties(&prop, i);
    printf("Device Number: %d\n", i);
    printf("  Device name: %s\n", prop.name);
    printf("  Memory Clock Rate (KHz): %d\n",
        prop.memoryClockRate);
    printf("  Memory Bus Width (bits): %d\n",
        prop.memoryBusWidth);
    printf("  Peak Memory Bandwidth (GB/s): %f\n\n",
        2.0*prop.memoryClockRate*(prop.memoryBusWidth/8)/1.0e6);
  }
}

void filter(Grid& grid, int R) 
{                                                                            
  auto middle = grid.N_logical[2] / 2; 
  auto box_size = 100.0f;                      
  auto delta_k = 2.0f * M_PI / box_size; 
                                    
  // Loop through k-box 
  for (int n_x = 0; n_x < grid.N_logical[0]; n_x++) { 
    float k_x;                                    
     
    if (n_x > middle) 
      k_x = (n_x - grid.N_logical[0]) * delta_k;                                      
    else                    
      k_x = n_x * delta_k; 
                         
    for (int n_y = 0; n_y < grid.N_logical[1]; n_y++) {                              
      float k_y;                                                     
                             
      if (n_y > middle)               
        k_y = (n_y - grid.N_logical[1]) * delta_k; 
      else                            
        k_y = n_y * delta_k;                           
                                                      
      for (int n_z = 0; n_z <= middle; n_z++) {                            
        auto k_z = n_z * delta_k;         
                                            
        auto k_mag = sqrtf(k_x * k_x + k_y * k_y + k_z * k_z); 
                                                                                                          
        auto kR = k_mag * R; // Real space top-hat                                        
 
        if (kR > 1e-4) {
          // cuCmat(grid.cat(n_x, n_y, n_z), make_cuComplex((3.0 * (sinf(kR) / powf(kR, 3) - cosf(kR) / powf(kR, 2))), 0.0f));
          grid.cat(n_x, n_y, n_z).x *= 3.0 * (sinf(kR) / powf(kR, 3) - cosf(kR) / powf(kR, 2));
        }
      } 
    } 
  } // End looping through k box 
}

void transform_3d(const int dim)
{
  int N[3] = {dim, dim, dim};
  auto grid = Grid(N);

  cudaMallocManaged(&(grid.c), sizeof(cufftComplex)*grid.Ntot_complex);
  grid.r = (cufftReal*)(grid.c);
  
  for(int ii=0; ii<grid.Ntot_real; ++ii) {
    grid.r[ii] = 1.;
  }

  int target[3] = {(int)(dim/2), (int)(dim/2), (int)(dim/2)};
  grid.rat(target[0], target[1], target[2]) = 10.;
  fmt::print("value = {}\n", grid.rat(target[0], target[1], target[2]));

  // Create a 3D FFT plan.
  cufftHandle plan;
  if (cufftPlan3d(&plan, N[0], N[1], N[2], CUFFT_R2C) != CUFFT_SUCCESS){
    fmt::print(stderr, "CUFFT error: Plan creation failed");
    return;	
  }	

  // Use the CUFFT plan to transform the signal in place.
  if (cufftExecR2C(plan, grid.r, grid.c) != CUFFT_SUCCESS){
    fprintf(stderr, "CUFFT error: ExecR2C Forward failed");
    return;	
  }

  if (cudaDeviceSynchronize() != cudaSuccess){
    fprintf(stderr, "Cuda error: Failed to synchronize\n");
    return;	
  }	

  cufftDestroy(plan);

  fmt::print("value = {}\n", grid.rat(target[0], target[1], target[2]));
  
  if (cufftPlan3d(&plan, N[0], N[1], N[2], CUFFT_C2R) != CUFFT_SUCCESS){
    fmt::print(stderr, "CUFFT error: Plan creation failed");
    return;	
  }	

  if (cufftExecC2R(plan, grid.c, grid.r) != CUFFT_SUCCESS){
    fprintf(stderr, "CUFFT error: ExecC2R Reverse failed");
    return;	
  }
  
  if (cudaDeviceSynchronize() != cudaSuccess){
    fprintf(stderr, "Cuda error: Failed to synchronize\n");
    return;	
  }	
  
  cufftDestroy(plan);

  auto norm = 1.0f/(float)grid.Ntot_logical;
  for(int ii=0; ii<grid.Ntot_real; ++ii) {
    grid.r[ii] *= norm;
  }

  fmt::print("value = {}\n", grid.rat(target[0], target[1], target[2]));

  cudaFree(grid.c);
}
